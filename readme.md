### Хакатон СтарЛайн 2023

Репозиторий содержит решение квалификационного задания хакатона, проводимого
НПО СтарЛайн в 2023 году.

### Cборка решения

1. Для запуска решения выполните действия по установке ПО из [readme с исходным заданием.](docs/README.md)

2. Склонируйте репозиторий и перейдите в его корневую директорию

```
git clone https://gitlab.com/robo_team401/hackathon-2023
cd hackathon_2022
```

3. Соберите и запустите докер контейнер с дополнительными зависимостями решения

```
cd simulation/docker; bash build.bash && bash run.bash
```

4. Запустите bash-сессию в конейнере:

```
bash into.bash
```

5. Перейдите в рабочую директорию и соберите проект:

```
cd workspace/; colcon build --symlink-install
```

6. После первой сборки окружения требуется переоткрыть bash-сессию в контейнере,
чтобы собранные пакеты стали видимы в системе.

### Запуск решения 

Запустите следующие launch-файлы:

```
ros2 launch survey ozyland.launch.py
```

```
ros2 launch bereg_nav bereg_bringup.launch.py
```

```
ros2 launch bereg_nav bereg_nav.launch.py
```

Дождитесь вывода ```Creating bond timer...``` и запустите пакет исследователя: 

```
ros2 launch bereg_nav bereg_explore.launch.py
```

Для мониторинга полученных кодов слушайте следующий топик: 

```
ros2 topic echo /qr_code_list
```

---

### Используемые пакты 

- qr_reader - пакет читает данные с камеры, распознает qr коды использую библиотеку [zbar](https://github.com/ZBar/ZBar), фильтует и публикует данные в топик /qr_code_list

- explore - пакет для исследования карты, по сетке ищет неизведанные участки кары и пишет точку в actrion navigate_to_pose

    [версия под ros2](https://github.com/hrnr/m-explore)

    [roswiki](http://wiki.ros.org/explore_lite)

- slam_toolbox - пакет для одновременного картирования и локализации на карте

    [версия под ros2](https://github.com/SteveMacenski/slam_toolbox)

    [roswiki](http://wiki.ros.org/slam_toolbox)

- Пакеты стека навигации

    [Документация](https://navigation.ros.org/)