#!/usr/bin/env python3

from launch import LaunchDescription

from launch_ros.actions import Node



def generate_launch_description():

    return LaunchDescription([
        
        Node(package    = "qr_reader", 
             executable = "qr_reader_node",
             remappings=[
                         ('/image', '/camera/image_raw'),
                        ],
             output     = "screen") 
    ])

