#include <string>
#include <unordered_map>
#include <memory>
#include <functional>
#include <chrono>

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp/timer.hpp"

#include "sensor_msgs/msg/image.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/bool.hpp"
#include "qr_reader/msg/result.hpp"

#include "cv_bridge/cv_bridge.h"
#include "zbar.h"


using namespace std::chrono_literals;


std::unordered_map<int, std::string> RESULT{
    {1,  "-"},
    {2,  "-"},
    {3,  "-"},
    {4,  "-"},
    {5,  "-"},
    {6,  "-"},
    {7,  "-"},
    {8,  "-"},
    {9,  "-"},
    {10, "-"},
    {11, "-"},
    {12, "-"},
    {13, "-"},
    {14, "-"},
    {15, "-"}
};


class QrReaderNode : public rclcpp::Node{
public:
  QrReaderNode();

private:
  void img_cb(sensor_msgs::msg::Image::ConstSharedPtr msg);

  void qr_cb();

  void qr_filter(std::string new_code);

private:
  rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr camera_sub_;

  rclcpp::Publisher<qr_reader::msg::Result>::SharedPtr qr_code_pub_;

  rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr qr_result_pub_;

  zbar::ImageScanner scanner_;

private:
  rclcpp::TimerBase::SharedPtr timer_;

  bool is_all_qr_funded_ = false;
  
  std::string prev_code_ = "";
  
  int fill_counter_ = 0;

};


QrReaderNode::QrReaderNode(): Node("QrReaderNode")
{
  scanner_.set_config(zbar::ZBAR_QRCODE, zbar::ZBAR_CFG_ENABLE, 1);
  scanner_.enable_cache(true);

  camera_sub_ = this->create_subscription<sensor_msgs::msg::Image>(
    "image", 10, std::bind(&QrReaderNode::img_cb, this, std::placeholders::_1));
  RCLCPP_INFO(this->get_logger(), "Subscribed to [%s]", camera_sub_->get_topic_name()); 

  qr_code_pub_ = this->create_publisher<qr_reader::msg::Result>("qr_code_list", 10);
  RCLCPP_INFO(this->get_logger(), "Publishing data to [%s]", qr_code_pub_->get_topic_name()); 

  qr_result_pub_ = this->create_publisher<std_msgs::msg::Bool>("is_qr_all_find", 10);
  RCLCPP_INFO(this->get_logger(), "Publishing state to [%s]", qr_result_pub_->get_topic_name()); 

  qr_cb();
}


void QrReaderNode::qr_cb(){   
    auto msg = qr_reader::msg::Result();

    msg.qr_1  = RESULT[1];
    msg.qr_2  = RESULT[2];
    msg.qr_3  = RESULT[3];
    msg.qr_4  = RESULT[4];
    msg.qr_5  = RESULT[5];
    msg.qr_6  = RESULT[6];
    msg.qr_7  = RESULT[7];
    msg.qr_8  = RESULT[8];
    msg.qr_9  = RESULT[9];
    msg.qr_10 = RESULT[10];
    msg.qr_11 = RESULT[11];
    msg.qr_12 = RESULT[12];
    msg.qr_13 = RESULT[13];
    msg.qr_14 = RESULT[14];
    msg.qr_15 = RESULT[15];

    auto state = std_msgs::msg::Bool();
    state.data = this->is_all_qr_funded_;

    qr_code_pub_->publish(msg);
    qr_result_pub_->publish(state);
}


void QrReaderNode::img_cb(sensor_msgs::msg::Image::ConstSharedPtr image)
{ 
  cv_bridge::CvImageConstPtr cv_image;
  cv_image = cv_bridge::toCvShare(image, "mono8");

  //gray to binary
  cv::Mat img_bin;
  cv::threshold(cv_image->image, img_bin, 30, 200, 0);

  // cv::imshow("Display window", img_bin);  // DEBUG
  // cv::waitKey(1);  // DEBUG

  zbar::Image zbar_image(cv_image->image.cols, cv_image->image.rows, "Y800", (uchar *)img_bin.data,
    cv_image->image.cols * cv_image->image.rows);
  scanner_.scan(zbar_image);

  auto it_start = zbar_image.symbol_begin();
  auto it_end = zbar_image.symbol_end();
    for (zbar::Image::SymbolIterator symbol = it_start; symbol != it_end; ++symbol) {
      std::string code = symbol->get_data();
      // RCLCPP_INFO(get_logger(), "Info at the moment: [%s]", code.c_str());  // DEBUG
      qr_filter(code);
    }  

  zbar_image.set_data(NULL, 0);
}


void QrReaderNode::qr_filter(std::string new_code){
    if (this->prev_code_ == new_code) {return;}
        
    // parse qr code number
    int qr_num = 0;
    std::string str_num = "";

    for(const char symbol: new_code){
        if(symbol == '.') break;
        str_num += {symbol};
    }
    qr_num = stoi(str_num);
    
    // fill new qr code
    if (RESULT[qr_num] == "-") {
        RESULT[qr_num] = new_code;
        fill_counter_++;
        
        if (fill_counter_ == 15) {
          is_all_qr_funded_ = true;
        }

        qr_cb();
    }
}


int main(int argc, char ** argv)
{
  rclcpp::init(argc, argv);

  rclcpp::spin(std::make_shared<QrReaderNode>());

  rclcpp::shutdown();

  return 0;
}